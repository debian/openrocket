#!/bin/sh
#
#	clean upstream source for Debian policy compliance
#	Copyright 2021 by Bdale Garbee.  GPL v3 or any later version.
#

# elide non-free logo files from html docs
git rm -f core/web/html/project-support.jpg
git rm -f core/web/html/valid-xhtml10.png

# elide pre-built class library jar files in favor of Debian packages
git rm -f core/lib/annotation-detector-3.0.5.jar

exit 0
