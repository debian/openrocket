Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: openrocket
Source: https://openrocket.info

Files: *
Copyright: 2007-2011 Sampo Niskanen <sampo.niskanen@iki.fi>
License: GPL-3+
    The licensors grant additional permission to package this Program, or
    any covered work, along with any non-compilable data files (such as
    thrust curves or component databases) and convey the resulting work.

Files: debian/*
Copyright: 2010-2020 Bdale Garbee <bdale@gag.com>
License: GPL-3+

Files: core/resources/datafiles/thrustcurves/thrustcurves.ser
Copyright: 1997-2001 Tripoli Motor Testing
License: Freely_Redistributable
 This file contains motor performance data obtained by upstream from 
 thrustcurve.org, a resource maintained by and for the hobby rocketry 
 community.
 .
 While those who author motor data files may assert copyright, very few do, 
 and any data submitted to thrustcurve.org is intended and understood to be 
 freely redistributable.

License: GPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 3 dated June, 2007, or (at
 your option) any later version.
 .
 On Debian systems, the complete text of version 3 of the GNU General
 Public License can be found in '/usr/share/common-licenses/GPL-3'.
